# README 

# How to setup CI/CD

## What CI/CD

CI/CD:
* https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd
* https://about.gitlab.com/features/gitlab-ci-cd/
* https://docs.gitlab.com/ee/ci/quick_start/

## Prepare Tools
* Create heroku account
* Get API Key from [heroku account](https://dashboard.heroku.com/account), save api key for later use
* Create application, set name app as domain from your product. Name must unique.
* Create file `.gitlab-ci.yml` on your apps root, fill with [example](.gitlab-ci.yml)

## Section for [gitlab-ci.yml](.gitlab-ci.yml)

There are two main jobs:
* compile code for node js like this: `npm run build`
* push code to heroku using gem dpl
* set limitation branch by enable: `only` 

## Any question
Find me on telegram @tuanpembual
